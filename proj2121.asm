; By Ashley Rodrigues and Waseem Sanjeev UNSw
; The program gets input from keypad and displays its ascii value on the
; LED bar
.include "m2560def.inc"

.def row = r16 ; current row number
.def col = r17 ; current column number
.def rmask = r18 ; mask for current row during scan
.def cmask = r19 ; mask for current column during scan
.def temp1 = r20
.def temp2 = r21
.def temp3 = r22
.def temp = r23

.def timerCount = r24
.equ PORTADIR = 0xF0 ; PD7-4: output, PD3-0, input
.equ INITCOLMASK = 0xEF ; scan from the rightmost column,
.equ INITROWMASK = 0x01 ; scan from the top row
.equ ROWMASK = 0x0F ; for obtaining input from Port D
.equ INCCONSTANT = 30 
.equ TWO_AND_A_HALF_SECONDS = 19530
.equ ONE_SECOND = 7812;
.equ OPEN = 'O'
.equ CLOSED = 'C'

.equ LCD_RS = 7
.equ LCD_E = 6
.equ LCD_RW = 5
.equ LCD_BE = 4

.equ ENTRY = 0 ; Entry mode
.equ RUNNING = 1 ;Running Mode
.equ PAUSED = 2 ; Paused Mode
.equ FINISHED = 3 ;Finished Mode
.equ POWER = 4 ; PowerMode

.macro lcd_set
	sbi PORTA, @0
.endmacro
.macro lcd_clr
	cbi PORTA, @0
.endmacro

.macro clearLCD
do_lcd_command 0b00000001 ; clear display
;do_lcd_command 
.endmacro

.macro show_time	
	clearLCD
	do_lcd_data tsplitMH
	do_lcd_data tsplitML
	do_lcd_char ':'
	do_lcd_data tsplitSH
	do_lcd_data tsplitSL
	do_lcd_command 0b01010000 ; set cursor to line 2
	do_lcd_data doorState
	do_lcd_command 0b11001111 ; set cursor to line 2
	do_lcd_data doorState
.endmacro

.macro show_state
	clearLCD
	do_lcd_command 0b11000000 ; set cursor to line 2
	do_lcd_data turnTablePos	
	do_lcd_command 0b11001111 ; set cursor to line 2
	do_lcd_data doorState

.endmacro

.macro IOSTORE
.if @0 < 64
out @0, @1
.else
sts @0, @1
.endif
.endmacro
;IOSTORE PORTK, r16

.macro do_lcd_command
	push r16
	ldi r16, @0
	call lcd_command
	call lcd_wait
	pop r16
.endmacro

.macro do_lcd_data
	push r16
	lds r16, @0
	subi r16, -48
	call lcd_data
	call lcd_wait
	pop r16
.endmacro

.macro do_lcd_char
	push r16
	ldi r16, @0
	;subi r16, -48
	call lcd_data
	call lcd_wait
	pop r16
.endmacro

.equ SHIFT_NUM = 32


.dseg
counter1: .byte 2
milliCounter: .byte 1
buttonPush: .byte 1
lastScan: .byte 1 
output: .byte 1

operation: .byte 1
prevOp: .byte 1
value: .byte 1
valEmpty: .byte 1

num: .byte 1
num1: .byte 1
num2: .byte 1 

mode: .byte 1

; Storing time
timeM: .byte 1 
timeS: .byte 1

; Minutes digits stored individually
tsplitML: .byte 1
tsplitMH: .byte 1

; Seconds digits stored individually
tsplitSL: .byte 1
tsplitSH: .byte 1

prevKey: .byte 1 ; previously pushed key

powerLvl: .byte 1 ;power level

secondCounter: .byte 2 ;counts up to 1 secondCounter

trnTbleCounter: .byte 2 ;

motorRunTime: .byte 2

doorState: .byte 1 ;state of the door initially closed

turntablePos: .byte 1 ;turntable position

accumulator : .byte 1

intActive: .byte 1 ; boolean value indicating when the interrupt is active	
	
MillCounter:
	.byte 2 

intService: .byte 1 ;interrupt being serviced	
currInt: .byte 1
pbOutput: .byte 1

trnTablePos: .byte 4

.cseg
jmp RESET

.org INT0addr 
jmp EXT_INT0

.org INT1addr
jmp EXT_INT1

.org INT2addr 
jmp EXT_INT2

.org OVF2addr
jmp Timer2OVF

.org OVF1addr
	jmp Timer1OVF ; Jump to the interrupt handler for

				; Timer0 overflow.

.org OVF0addr
jmp Timer0OVF

.org OC5Aaddr
jmp Timer5AOC

RESET:
	ldi temp1, low(RAMEND) ; initialize the stack
	out SPL, temp1
	ldi temp1, high(RAMEND)
	out SPH, temp1
	ldi temp1, PORTADIR ; PA7:4/PA3:0, out/in
	IOSTORE DDRK, temp1
	ser temp1 ; PORTC is output
	out DDRC, temp1
	out PORTC, temp1
	ser r16
	out DDRF, r16
	out DDRA, r16
	clr r16
	out PORTF, r16
	out PORTA, r16

	ldi temp, (2 << ISC00) ; set INT0 as falling
	sts EICRA, temp ; edge triggered interrupt
	in temp, EIMSK ; enable INT0
	ori temp, (1<<INT0)
	ori temp, (1<<INT1)
	ori temp, (1<<INT2)
	out EIMSK, temp
	
	do_lcd_command 0b00111000 ; 2x5x7
	call sleep_5ms
	do_lcd_command 0b00111000 ; 2x5x7
	call sleep_1ms
	do_lcd_command 0b00111000 ; 2x5x7
	do_lcd_command 0b00111000 ; 2x5x7
	do_lcd_command 0b00001000 ; display off?
	do_lcd_command 0b00000001 ; clear display
	do_lcd_command 0b00000110 ; increment, no display shift
	do_lcd_command 0b00001110 ; Cursor on, bar, no blink
	clr temp1
	sts counter1, temp1
	sts counter1+1, temp1
	sts buttonPush, temp1
	sts output, temp1
	sts operation, temp1
	sts value, temp1
	sts valEmpty, temp1;
	sts lastScan, temp1
	sts accumulator, temp1;
	sts operation, temp1
	sts prevOp, temp1
	sts mode, temp1
	sts MillCounter, temp1
	sts MillCounter+1, temp1
	sts intActive, temp1
	sts pbOutput, temp1
	sts timeM, temp1
	sts timeS, temp1
	sts secondCounter, temp1
	sts secondCounter+1, temp1
	sts motorRunTime, temp1
	sts motorRunTime+1, temp1
	sts tsplitML, temp1
	sts tsplitMH, temp1
	sts tsplitSL, temp1
	sts tsplitSH, temp1
	sts trnTbleCounter, temp1
	sts trnTbleCounter+1, temp1

	ldi temp1, 'C'
	sts doorState, temp1
	ldi temp1, '-'
	sts turntablePos, temp1
	ldi temp1, 3
	sts powerLvl, temp1
	ldi temp1, low(7812)
	sts motorRunTime, temp1
	ldi temp1, high(7812)
	sts motorRunTime+1, temp1
	clr temp1
	show_state
	rjmp main

	
Timer5AOC:
		reti

Timer1OVF: ; interrupt subroutine to Timer1
	push temp
	in temp, SREG
	push temp ; Prologue starts.
	push r25
	push r24 ; Prologue ends. 
			; Load the value of the temporary counter.

	lds r24, MillCounter
	lds r25, MillCounter+1
	adiw r25:r24, 1
	sts MillCounter, r24
	sts MillCounter+1, r25
	cpi r24, low(6)
	ldi temp, high(6)
	cpc r25, temp
	breq checkInterrupt
	jmp endTimer1	

checkInterrupt:
	clr temp
	sts MillCounter, temp
	sts MillCounter+1, temp
	lds temp, intActive
	cpi temp, 1
	brne disableTimer
	ldi temp, 0
	sts intActive, temp
	rjmp endTimer1

disableTimer:
	clr temp
	sts TIMSK1, temp
	lds temp, intService
	cpi temp, 0
	breq actionExt0
	rjmp actionExt1
	;ldi temp, 1
	;sts pbOutput, temp

actionExt0:
	lds temp, doorState
	cpi temp, CLOSED
	in temp, SREG
	sbrc temp, SREG_Z
	jmp endTimer1
	;breq endTimer1
	ldi temp, CLOSED
	sts doorState, temp
	show_time
	rjmp endTimer1

actionExt1:
	lds temp, doorState
	cpi temp, OPEN
	in temp, SREG
	sbrc temp, SREG_Z
	jmp endTimer1
	ldi temp, OPEN
	sts doorState, temp
	show_time
	lds temp, mode
	cpi temp, RUNNING
	breq ROpenDoorRun
	cpi temp, FINISHED
	breq ROpenDoorFin
	rjmp endTimer1
	
ROpenDoorRun:

	ldi temp, PAUSED
	sts mode, temp
	clr temp
	sts TIMSK2, temp
	sts OCR5AL, temp
	;disable magnetron
	;show_time
	rjmp endTimer1

ROpenDoorFin:
	ldi temp, ENTRY
	sts mode, temp
	show_state
	rjmp endTimer1

endTimer1:
	pop r24 ; Epilogue starts;
	pop r25 ; Restore all conflict registers from the stack.
	pop temp
	out SREG, temp
	pop temp
	reti ; Return from the interrupt.
	
	
Timer0OVF:
	push temp
	push temp2
	push temp3
	lds XL, counter1
	lds XH, counter1 +1
	adiw XH:XL, 1
	sts counter1, XL
	sts counter1+1, XH
	cpi XL, low(351)
	ldi temp, high(351)
	cpc XH, temp
	breq check
	rjmp end_timer_int

check:
	lds temp2, lastScan
	lds temp3, buttonPush
	cp temp2, temp3
	breq sameButton
	clr temp2
	sts TIMSK0, temp2
	;cli
	ldi temp2, 1
	sts output, temp2

	rjmp end_timer_int

sameButton:
	clr temp2
	sts counter1, temp2
	sts counter1 +1, temp2
	rjmp end_timer_int

end_timer_int:
	pop temp3
	pop temp2
	pop temp
	reti

Timer2OVF:
	push temp1
	push temp2
	push r25
	push r24
	lds r24, motorRunTime
	lds r25, motorRunTime+1
	adiw r25:r24, 1
	sts motorRunTime, r24
	sts motorRunTime, r25
	mov temp1, r24
	mov temp2, r25
	lds r24, trnTbleCounter
	lds r25, trnTbleCounter+1
	adiw r25:r24, 1
	sts trnTbleCounter, r24
	sts trnTbleCounter+1, r25
	lds XL, secondCounter
	lds XH, secondCounter+1
	adiw XH:XL, 1
	sts secondCounter, XL
	sts secondCounter+1, XH

	cp temp1, XL
	cpc temp2, XH
	in r24, SREG
	sbrs r24, SREG_Z
	rjmp isOneSecond
	clr temp1
	sts OCR5AL, temp1

isOneSecond:
	cpi XL, low(7812)
	ldi temp1, high(7812)
	cpc XH, temp1
	;rjmp updateTimeCounters
	breq updateTimeCounters
	rjmp checkTrnTable
	
updateTimeCounters: ; update the countdown timers

	show_time ; updates time on display
	clr temp1
	sts secondCounter, temp1 ;reset values in secCounter
	sts secondCounter+1, temp1
	;sts motorRunTime, temp1
	;sts motorRunTime+1, temp1
	lds temp1, timeS
	cpi temp1, 0
	breq updateMinute
	dec temp1
	sts timeS, temp1
	rjmp updateTimeDisplay

updateMinute:
	lds temp1, timeM
	cpi temp1, 0
	in temp2, SREG
	sbrc temp2, SREG_Z
	jmp finishTimer01
	;breq finishTimer01
	dec temp1
	sts timeM, temp1
	ldi temp1, 59
	sts timeS, temp1
	rjmp updateTimeDisplay

updateTimeDisplay:
	lds temp1, timeM
	sts accumulator, temp1
	call numToUnit
	lds temp1, num
	lds temp2, num1
	sts tsplitMH, temp2
	sts tsplitML, temp1

	lds temp1, timeS
	sts accumulator, temp1
	call numToUnit
	lds temp1, num
	lds temp2, num1
	sts tsplitSH, temp2
	sts tsplitSL, temp1
	show_time
	lds temp1, timeM
	lds temp2, timeS
	add temp1, temp2
	cpi temp1, 0
	breq finishTimer01
	ldi temp1, 0b11011111
	out PORTC, temp1
	ldi temp1, 0x4A
	sts OCR5AL, temp1
	;change LCD Output
	rjmp checkTrnTable
	rjmp end_timer01_int

finishTimer01: ;countdown has finished so stop the timer
	clr temp1
	sts TIMSK2, temp1
	sts OCR5AL, temp1
	ldi temp1, FINISHED
	sts mode, temp1
	clearLCD
	do_lcd_char 'D'
	do_lcd_char 'o'
	do_lcd_char 'n'
	do_lcd_char 'e'
	do_lcd_command 0b11000000 ; set cursor to line 2
	do_lcd_char 'R'
	do_lcd_char 'e'
	do_lcd_char 'm'
	do_lcd_char 'o'
	do_lcd_char 'v'
	do_lcd_char 'e'
	do_lcd_char ' '
	do_lcd_char 'f'
	do_lcd_char 'o'
	do_lcd_char 'o'
	do_lcd_char 'd'
	rjmp end_timer01_int
	
checkTrnTable:
	cpi r24, low(TWO_AND_A_HALF_SECONDS)
	ldi temp1, high(TWO_AND_A_HALF_SECONDS)
	cpc r25, temp1
	brne end_timer01_int

	clr temp1
	sts trnTbleCounter, temp1
	sts trnTbleCounter+1, temp1
	;update turntable stuff
	ldi temp1, 0b11001100
	out PORTC, temp1
	rjmp end_timer01_int	

end_timer01_int:
	;ldi temp1, 0b11100011
	;out PORTC, temp1
	pop r24
	pop r25
	pop temp2
	pop temp1
	reti
	

main:
	lds temp2, output
	cpi temp2, 1
	in temp1, SREG
	sbrc temp1, SREG_Z 
	rjmp convert_end
	ldi cmask, INITCOLMASK ; initial column mask
	clr col ; initial column


colloop:
	cpi col, 4	
	breq finishScan ; If all keys are scanned, repeat.
	IOSTORE PORTK, cmask ; Otherwise, scan a column.
	ldi temp1, 0xFF ; Slow down the scan operation.
	rjmp delay

finishScan:
	push temp1
	clr temp1
	sts lastScan, temp1
	pop temp1
	rjmp main

delay: dec temp1
	brne delay
	lds temp1, PINK ; Read PORTA
	andi temp1, ROWMASK ; Get the keypad output value
	cpi temp1, 0xF ; Check if any row is low
	breq nextcol
	; If yes, find which row is low
	ldi rmask, INITROWMASK ; Initialize for row check
	clr row ; 
	
rowloop:
	cpi row, 4
	breq nextcol ; the row scan is over.
	mov temp2, temp1
	and temp2, rmask ; check un-masked bit
	breq convert ; if bit is clear, the key is pressed
	inc row ; else move to the next row
	lsl rmask
	jmp rowloop


nextcol: ; if row scan is over
	lsl cmask
	inc col ; increase column value
	jmp colloop ; go to the next column
	
convert:
	cpi col, 3 ; If the pressed key is in col.3
	breq letters ; we have a letter
	; If the key is not in col.3 and
	cpi row, 3 ; If the key is in row3,
	breq symbols ; we have a symbol or 0
	mov temp1, row ; Otherwise we have a number in 1-9
	lsl temp1
	add temp1, row
	add temp1, col ; temp1 = row*3 + col
	subi temp1, -'1' ; Add the value of character �1�
	jmp timer_status

letters:
	ldi temp1, 'A'
	add temp1, row ; Get the ASCII value for the key
	jmp timer_status

symbols:
	cpi col, 0 ; Check if we have a star
	breq star
	cpi col, 1 ; or if we have zero
	breq zero
	ldi temp1, '#' ; if not we have hash
	jmp timer_status


star:
	ldi temp1, '*' ; Set to star
	jmp timer_status

zero:
	ldi temp1, '0' ; Set to zero
	jmp timer_status

convert_end:
	lds temp1,buttonPush
	;out PORTC, temp1
	; handles each individual mode accordingly

	lds temp2, doorState
	cpi temp2, OPEN
	in temp2, SREG
	sbrc temp2, SREG_Z
	jmp endButtonCheck		

	lds temp2, mode
	cpi temp2, ENTRY
	in temp2, SREG
	sbrc temp2, SREG_Z
	jmp handleEMode

	lds temp2, mode
	cpi temp2, RUNNING
	in temp2, SREG
	sbrc temp2, SREG_Z
	jmp handleRMode
	
	lds temp2, mode
	cpi temp2, PAUSED
	in temp2, SREG
	sbrc temp2, SREG_Z
	jmp handlePMode
	
	lds temp2, mode
	cpi temp2, FINISHED
	in temp2, SREG
	sbrc temp2, SREG_Z
	jmp handleFMode
	
	lds temp2, mode
	cpi temp2, POWER
	in temp2, SREG
	sbrc temp2, SREG_Z
	jmp handlePwrMode
	
	subi temp2, 48
	sts buttonPush, temp2
	lds temp3, value
	ldi temp2, 10
	mul temp3, temp2
	lds temp2, buttonPush
	mov temp3, r0
	add temp3, temp2
	sts value, temp3
	;rcall setStart
	do_lcd_data buttonPush
	rjmp convert_rest;

handleEMode: ;Used to handle key pushes when in entry mode
	cpi temp1, 'A'
	in temp2, SREG
	sbrc temp2, SREG_Z
	jmp ESetPwrMode
	cpi temp1, '0'
	brge isLessThn
	cpi temp1, '#'
	breq wipeTime
	cpi temp1, '*'
	in temp2, SREG
	sbrc temp2, SREG_Z
	rjmp startRunningMode
	rjmp endButtonCheck

; if the ascii value of the key is less than or equal to nine
; it must be a number
isLessThn:
	cpi temp1, ':'
	brlt keyIsNumber
	rjmp endButtonCheck
	
; branches here if key pressed is a number	
keyIsNumber:
	lds temp1, tsplitMH	
	cpi temp1, 0b00000000
	in temp1, SREG
	sbrc temp1, SREG_Z
	jmp addDigit
	rjmp endButtonCheck

; resets the current time displayed to zero
wipeTime:
	clr temp1
	sts tsplitMH, temp1
	sts tsplitML, temp1
	sts tsplitSH, temp1
	sts tsplitSL, temp1
	sts timeM, temp1
	sts timeS, temp1
	show_time
	;update the display
	rjmp endButtonCheck

; changes mode to running mode	
; still need to do magnetron and turntable
startRunningMode:
	ldi temp1, RUNNING
	sts mode, temp1
	clr temp1
	lds temp2, timeM
	add temp1, temp2
	lds temp2, timeS
	add temp1, temp2
	cpi temp1, 0b00000000
	in temp2, SREG
	sbrs temp2, SREG_Z	
	rjmp enable_timer02
	ldi temp1, 1
	sts timeM, temp1
	sts tsplitML, temp1
	clr temp1
	sts timeS, temp1
	sts tsplitSL, temp1
	sts tsplitSH, temp1
	sts tsplitMH, temp1
	show_time
	;rjmp endButtonCheck
enable_timer02:	
	clr temp1
	sts secondCounter, temp1
	sts secondCounter+1, temp1
	ldi temp1, 0b00000000
	IOSTORE TCCR2A, temp1
	ldi temp1, 0b00000010
	IOSTORE TCCR2B, temp1 ; Prescaling value=8
	ldi temp1, 1<<TOIE2 ; = 128 microseconds
	sts TIMSK2, temp1 ; T/C0 interrupt enable
;		sei
;	rjmp endButtonCheck	
enable_timer05:
	ldi temp, 0b00001000
	sts DDRL, temp ; Bit 3 will function as OC5A.
	ldi temp, 0x4A ; the value controls the PWM duty cycle
	sts OCR5AL, temp
	clr temp
	sts OCR5AH, temp
	; Set the Timer5 to Phase Correct PWM mode.
	ldi temp, (1 << CS50)
	sts TCCR5B, temp
	ldi temp, (1<< WGM50)|(1<<COM5A1)
	sts TCCR5A, temp
	ldi temp, 1<<OCIE5A ; = 128 microseconds
	sts TIMSK5, temp ; T/C0 interrupt enable
	sei
	rjmp endButtonCheck	
		
	
; adds digit to time display and updates LCD	
addDigit:
	lds temp1, buttonPush
	;sts timeS, temp1
	lds temp1, tsplitML
	lds temp2, tsplitSH
	ldi temp3, 10	

	sts tsplitMH, temp1
	sts tsplitML, temp2
	
	mul temp1, temp3
	mov temp1, r0
	add temp1, temp2
	sts timeM, temp1
	 
	lds temp1, tsplitSL
	lds temp2, buttonPush
	sts tsplitSH, temp1
	subi temp2, 48
	sts tsplitSL, temp2
	mul temp1, temp3
	mov temp1, r0
	add temp1, temp2
	sts timeS, temp1
	; do an update display command her
	show_time
	;do_lcd_command  0b01000100
	;do_lcd_char 'b'	
	rjmp endButtonCheck
	
ESetPwrMode:
	ldi temp2, POWER
	sts mode, temp2
	rjmp endButtonCheck
		
handlePwrMode: ;Used to set the power level in entry mode
	cpi temp1, '1'
	breq setPower1
	cpi temp1, '2'
	breq setPower2
	cpi temp1, '3'
	breq setPower3
	cpi temp1, '#'
	breq PwrSetEntryMode
	rjmp endButtonCheck

setPower1: ;sets the power level to 1
	subi temp1, 48
	sts powerLvl, temp1
	ldi temp1, 0b00000011
	out PORTC, temp1
	ldi temp1, low(1953)	
	sts motorRunTime, temp1
	ldi temp1, high(1953)
	sts motorRunTime+1, temp1			
	rjmp endButtonCheck

setPower2: ;sets the power level to 2
	subi temp1, 48
	sts powerLvl, temp1
	ldi temp1, 0b00001111
	out PORTC, temp1
	ldi temp1, low(3906)	
	sts motorRunTime, temp1
	ldi temp1, high(3906)
	sts motorRunTime+1, temp1	
	rjmp endButtonCheck

setPower3: ;sets the power level to 3
	subi temp1, 48
	sts powerLvl, temp1
	ser temp1
	out PORTC, temp1	
	ldi temp1, low(7812)	
	sts motorRunTime, temp1
	ldi temp1, high(7812)
	sts motorRunTime+1, temp1	
	rjmp endButtonCheck
	
;Displays the power mode value to the leds	


PwrSetEntryMode:
	ldi temp1, ENTRY
	sts mode, temp1
	rjmp endButtonCheck
		
handleRMode:
	cpi temp1, 'D'
	breq  subtractConstant
	cpi temp1, 'C'
	breq addConstant
	cpi temp1, '*'
	breq RStart
	cpi temp1, '#'
	breq RStop
	rjmp endButtonCheck

; subtract 30 seconds from time if possible
subtractConstant:
	lds temp1, timeS
	out PORTC, temp1
	cpi temp1, INCCONSTANT
	brge moreThn30Secs
	lds temp2, timeM
	cpi temp2, 1
	brge moreThn0Min
	rjmp endButtonCheck

; if there are more than 30 seconds in time seconds counter	
moreThn30Secs:	
	subi temp1, INCCONSTANT
	sts timeS, temp1
	rjmp endButtonCheck

RStart:
	lds temp1, timeM
	cpi temp1, 99
	breq moreThnMaxMins
	inc temp1
	sts timeM, temp1
	rjmp updateTime
	
RStop:
	ldi temp1, PAUSED
	sts mode, temp1
	clr temp1
	sts TIMSK2, temp1 
	sts OCR5AL, temp1
	rjmp endButtonCheck
	
; if there is more than 0 min on second counter
moreThn0Min:
	dec temp2
	ldi temp3, 60
	subi temp3, INCCONSTANT
	add temp3, temp1
	sts timeM, temp2
	sts timeS, temp3
	rjmp updateTime

addConstant:
	lds temp1, timeS
	cpi temp1, 70
	brge moreThn70Secs
	subi temp1, -INCCONSTANT
	sts timeS, temp1
	rjmp updateTime
	
moreThn70Secs:
	lds temp2, timeM
	cpi temp2, 99
	breq moreThnMaxMins
	subi temp1, -INCCONSTANT
	subi temp1, 2*INCCONSTANT
	inc temp2
	sts timeM, temp2
	sts timeS, temp1
	rjmp updateTime
	
moreThnMaxMins:
	ldi temp1, 99
	sts timeM, temp1
	sts timeS, temp1
	rjmp updateTime

updateTime:
	show_time
	rjmp endButtonCheck

; paused mode must turn off magnetron, and turntable
; and timer to display
handlePMode:
	cpi temp1, '*'
	breq PStart
	cpi temp1, '#'
	breq PStop
	rjmp endButtonCheck
	
PStart:
	lds temp1, doorState
	cpi temp1, OPEN
	in temp1, SREG
	sbrc temp1, SREG_Z
	jmp endButtonCheck
	ldi temp1, RUNNING
	sts mode, temp1
	ldi temp1, 0b000000001
	sts TIMSK2, temp1
	ldi temp1, 0x4A ; the value controls the PWM duty cycle
	sts OCR5AL, temp1
	rjmp endButtonCheck

PStop:
	lds temp1, doorState
	cpi temp1, OPEN
	in temp1, SREG
	sbrc temp1, SREG_Z
	jmp endButtonCheck
	ldi temp1, ENTRY
	sts mode, temp1
	clr temp1
	sts timeM, temp1
	sts timeS, temp1
	sts tsplitMH, temp1
	sts tsplitML, temp1
	sts tsplitSH, temp1
	sts tsplitSL, temp1
	show_state
	rjmp endButtonCheck
			
handleFMode:

endButtonCheck: ;once finished handling the button pressed returns to main to continue scanning
	clr temp2
	sts output, temp2
	sts buttonPush, temp2
	rjmp main

timer_status:
	push temp2
	sts lastScan, temp1
	lds temp2, buttonPush
	cpi temp2, 0
	breq enable_timer
	pop temp2
	rjmp main
;if timer is on turn it on
; if timer is not on leave it off

enable_timer:
	push temp
	sts buttonPush, temp1
	ldi temp, 0b00000000
	out TCCR0A, temp
	ldi temp, 0b00000010
	out TCCR0B, temp ; Prescaling value=8
	ldi temp, 1<<TOIE0 ; = 128 microseconds
	sts TIMSK0, temp ; T/C0 interrupt enable
	sei
	clr temp2
	sts counter1, temp2
	sts counter1 +1, temp2
	pop temp
	pop temp2
	rjmp main

convert_rest: ;restore all the stuff before returning
	clr temp2
	sts output, temp2
	sts buttonPush, temp2
	jmp main ; Restart main loop

	
EXT_INT0: 
	push temp
	in temp, SREG
	push temp
	ldi temp, 1
	sts intActive, temp
	ldi temp, 0
	sts intService, temp
	ldi temp, 0b0000001
	sts TCCR1B, temp ; Prescaling value=8
	ldi temp, 1<<TOIE1 ;=278 microseconds
	sts TIMSK1, temp ; T/C0 interrupt enable
	rjmp endExitInt

EXT_INT1:
	push temp
	in temp, SREG
	push temp

	ldi temp, 1
	sts intActive, temp	
	ldi temp, 1
	sts intService, temp
	ldi temp, 0b0000001
	sts TCCR1B, temp ; Prescaling value=8
	ldi temp, 1<<TOIE1 ; =278 microseconds
	sts TIMSK1, temp ; T/C0 interrupt enable
	rjmp endExitInt	

endExitInt:
	pop temp
	out SREG, temp
	pop temp
	reti	
	
EXT_INT2:
	reti	
	
; Store 1 byte number into 3 separate bytes inc
; data memory
numToUnit:
	push temp1
	push temp2
	push temp3
	in r31, SREG
	push r31
	clr temp1
	clr temp2
	clr temp3
	sts num, temp1
	sts num1, temp1
	lds temp1, accumulator 
	
tens:
	subi temp1, 10
	in r31, SREG
	andi r31, 0b00000100		
	cpi r31, 0b00000100
	breq units
	inc temp2
	rjmp tens

units:
	subi temp1, -10
	sts num, temp1
	sts num1, temp2
	pop r31
	out SREG, r31
	pop temp3
	pop temp2
	pop temp1
	ret
;


; Instructions to output data to LCD
; Send a command to the LCD (r16)
;

lcd_command:
	out PORTF, r16
	call sleep_1ms
	lcd_set LCD_E
	call sleep_1ms
	lcd_clr LCD_E
	call sleep_1ms
	ret

lcd_data:
	out PORTF, r16
	lcd_set LCD_RS
	call sleep_1ms
	lcd_set LCD_E
	call sleep_1ms
	lcd_clr LCD_E
	call sleep_1ms
	lcd_clr LCD_RS
	ret

lcd_wait:
	push r16
	clr r16
	out DDRF, r16
	out PORTF, r16
	lcd_set LCD_RW
lcd_wait_loop:
	call sleep_1ms
	lcd_set LCD_E
	call sleep_1ms
	in r16, PINF
	lcd_clr LCD_E
	sbrc r16, 7
	rjmp lcd_wait_loop
	lcd_clr LCD_RW
	ser r16
	out DDRF, r16
	pop r16
	ret

.equ F_CPU = 16000000
.equ DELAY_1MS = F_CPU / 4 / 1000 - 4
; 4 cycles per iteration - setup/call-return overhead

sleep_1ms:
	push r24
	push r25
	ldi r25, high(DELAY_1MS)
	ldi r24, low(DELAY_1MS)
delayloop_1ms:
	sbiw r25:r24, 1
	brne delayloop_1ms
	pop r25
	pop r24
	ret

sleep_5ms:
	call sleep_1ms
	call sleep_1ms
	call sleep_1ms
	call sleep_1ms
	call sleep_1ms
	ret

